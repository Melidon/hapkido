# 2. dan
## Azonos oldali csuklófogásból
### Chigi - _6 db_
- Azonos borda
- 45* Nyak vágás
- Lépcsős ököl+kézél
- Csuklótörés + torok
- Átbújós könyök
- Borda
### Chagi - _5 db_
- Sípcsont rúgás
- Combideg rúgás
- Yop Chagi
- Átbújós rúgás
- Térdrúgás
### Kokki - _12 db_
- Cowboyos alkarforgató
- Kétlépéses alkarforgató
- Csukló Z-kulcs
- Könyök Z-kulcs
- Szemből karfeszítés
- Szemből könyöktörés
- Átlós beugró könyöktörés
- Átlós beugró könyökhajlat
- Tenyeres csuklódobás
- Tenyeres vállfa
- Bicepszledöntés
- Táncos dobás
### Donchigi - _5 db_
- Két térdhajlat + Skorpió
- Félskorpió + Yop Chagi
- Menyasszony
- Sima csípő dobás
- Hátra ledöntés
## Átlós csuklófogásból
### Chigi - _2 db_
- Könyök fel
- Alkar torokra
### Chagi - _2 db_
- Sípcsont rúgás
- Combideg rúgás
### Kokki - _8 db_
- Csukló Z-kulcs
- Csukló Z-kulcs kézéllel
- Kézcserés karfeszítés
- Kézcserés könyöktörés
- Táncos dobás
- Bicepsz ledöntés
- Beugró könyöktörés
- Beugró könyökhajlat
### Donchigi - _4 db_
- Sima csípődobás
- Nyakas csípődobás
- Vállfa + Gáncsolás
- Hátra ledöntés
## Ütés ellen
### Chigi - _5 db_
- FEL-LE 2ütés
- FEL-LE dung jumok
- Kenem könyök másik borda
- Forgó könyök
- J-Csukyo + kézél + ütés
### Chagi - _5 db_
- Sípcsont
- Combideg
- Dollyo - NEM fog rá
- Yop Chagi
- Térdrúgás behúzott
### Kokki - _9 db_
- Piedone
- Alkarforgató
- X-Karfeszítés
- X-Beugró könyöktörés szemből
- Félkörös karfeszítés
- Félkörös beugró könyöktörés
- Csuklódobás másik jobb
- Vállfa másik jobb
- Bicepsz ledöntés
### Donchigi - _5 db_
- Sima csípő dobás
- Nyakas gáncs dobás
- S-Dobás
- X-Gáncsledöntés
- Tökös
## Yop Chagi ellen
### Chigi - 3 db
- Kedvenc könzök
- 1-2-3 üt combra
- 1 ujj ököl
### Chagi - _7 db_
- Ollózó Ap Chagi
- Ollózó Ap Cha-Bushigi
- Blokk + köríves rugás
- J-talp sípcsont
- Kifli rugás
- Majom
- Behúzott térdrúgás
### Kokki - _4 db_
- Lábkulcs hátra
- Térdhajlatbatérdelés
- Belső bokaideg alkarral
- Alálendít + csuklódobás
### Donchigi - _5 db_
- Sasszés lábsöprés
- Kis hullám
- Hátralökés
- Focis
- Vállon láb
## Chigo Chagi ellen
### Chigi - _3 db_
- Egz ujjasököl combra
- J-könyök combra
- B-térdrugás
### Chagi - _3 db_
- Yop Chagi
- Dwit Chagi
- Pande Dollyo Chagi
### Kokki - _2 db_
- Lábkulcs befelé
- Hónalj hátsó kulcs
### Donchigi - _2 db_
- Belső magamra húz
- Lábfeszítés
## Rátámádásos
### Chigi - _5 db_
- Könyök FEL
- Könyök ALKAR TOLÁS
- 2 Dung Jumok
- Hüvelykújj
- Csörike
### Chagi - _4 db_
- Ap Térdre
- Ap Ujjakra
- Combos forgatva
- Felkar forgatva
### Kokki - _8 db_
- Szemből Karfeszítés
- Szemből Könyöktörés
- Bicsepsz
- Táncos
- Beugró Könyöktörés
- Beugró Könyökhajlat
- Csukló letolás Z kulcs
- Cseles Rögbi
### Donchigi - _10 db_
- Két térdhajlat
- Félsokrpió
- Menyasszony
- Nyakas Gáncs dobás
- Gábor hátra ledöntés
- Válbahúz Hátra ledöntés
- Sima Csipő
- S dobás idegponttal
- Könyökhajlat dobás
- Ütős Télapó
## Ülésből
### Törökülés - _10 db_
#### Szemből
- Lágyékbeszúrás
- Térdkalács
- 2 könyök belülről
- 2 láb belülről
- 2 láb oldaltfekvésben
- Belső comb 1 ujj ököl
- Belső bokaidegpont
- Belső bokaidegpont Könyök-alkarral
#### Oldalt
- Térd Alkar
- Boka + Sarok térdhajlat
### Azonos oldali csuklófogásból - _4 db_
- Toll húz
- Könyök Z kulcs
- Tenyeres csukló Z kulcs
- Négyirányú - ujjemelő
### Átlós csuklófogásból - _4 db_
- Z kulcs ráfog
- Z kulcs kézél
- 4 irányú
- Csuklódobás
### Gallér - _3 db_
- Beszúrós Spanyol
- Alsó könyök feszítés
- Fejen átdobás
### Nyakkendő - _3 db_
- Könyök Z kulcs
- Csuklódobás
- Dupla Kézél
## Földön fekve fojtás
### Kokki - _5 db_
- Félhidas leforgatás
- Csondol + Csuklódobás
- Csondol Térdharapó Z kulcs
- Csondol + felülős Skorpió
- Csukló + könyök leforgatás
## Alsó egyenes kés ellen
### Makki - _10 db_
- Jobb emelő
- Bal emelő
- Bal Alkar BE
- Jobb Alkar BE
- Bal Alkar KI
- Jobb Alkar KI
- Bal kígyó
- Jobb kígyó
- X blokk
- Pillangó blokk
### Chigi - _8 db_
- Bal alálendít + Jobb 1ujjas ököl
- Csukló vágás
- Blokk + Jobb könyök
- Bloll + Könyök + Nyak
- Blokk + Kézél + Dollyo
- Forgó könyök gerinc
- Cséphadaró
- Belsőkézél
### Kokki - _6 db_
- X - Alárugós
- X - Csuklódobás
- Harakiri
- Négyirányú
- Karfeszítés
- Beugrós könyöktörés
### Donchigi - _5 db_
- Pillangós Gáncsledöntés
- Pillangós Külső Zsák
- Pillangós Csípődobás
- Pillangós Válldobás
- Pillangós Keresztcsípő dobás
## Felső kés ellen
### Chigi - _4 db_
- Bordalyukasztás
- Dollyo
- Io Yop
- Yop Hadan
### Kokki - _5 db_
- Csuklódobás
- Alárúgós
- Felsőkulcs Jobb ~ Vállbahúz
- Felsőkulcs Bal Jobb ~ 4irányú
- X karfeszítés
### Donchigi - _2 db_
- Külső Zsák
- X Gáncsledöntés
## Tenyeres kés ellen
### Chigi - _5 db_
- Könyök fejre
- Könyök bordára
- Kézél nyakra KI
- Kézél nyakra + Jobb Térdrúgás
- Dung Jumok Fel-Le
### Kokki - _3 db_
- Blokk + könyök + könyökforgató
- Rögbi 2
- Ököl betekerés
### Donchigi - _2 db_
- Gáncsledöntés
- Külső Zsák
## Fonák kés ellen
### Chigi - _3 db_
- Kedvenc könyök
- Kedvenc JObb Térdrúgás
- Könyök  + Nyakra csapás
### Kokki - _3 db_
- Karfeszítés
- Táncos
- Hasfogó Beugrú Könyöktörés
### Donchigi - _2 db_
- Hátsó lerántás
- Kiselefántos hátraszaltó
## Rövidbot
### Makki - _8 db_
- Fej
- B váll BE keresztbe
- J váll KI
- BE hárít könyök
- Borda (könyök) FEL
- Térd LE
- J Hátralép, J BE bokára
- Be hárít -2 lépés L állás
### Kibon - _8 db_
- Fej
- J halánték
- B halánték
- J BE hárít vízszintig Gyors
- J KI hárít vízszintig Gyors
- J láb visszahúz J lehárít
- J hátralép, J BE bokára
- Szúrás torokra
### Chigi - _8 db_
- Fej
- J Halánték
- B Halánték
- J Könyök
- B Könyök
- J Térd
- B Térd
- Szúrás gyomor és rövid felüt állra
