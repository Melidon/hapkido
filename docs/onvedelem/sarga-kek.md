# 8. gup
## Kabát ujj fogás
### Kokki - _3 db_
- Beszúrós karfeszítés
- Alkarforgató
- Lebukó
## Könyök fogás
### Kokki - _3 db_
- Lebukó
- W-s tenyértő
- Felütős hátsó kulcs
## Váll fogás
### Kokki - _3 db_
- Ide-oda karfeszítés
- Könyök Z-kulcs LE
- Térdharapó Z-kulcs
## Gallér fogás
### Chigi - _1 db_
- Könyöktörés
### Kokki - _1 db_
- Spanyol
### Donchigi - _1 db_
- Álltolás
## Popey
### Kokki - _3 db_
- Keresztlépéses csukló dobás
- Kígyós fejcsavar
- Hüvelykujj leszúrt csuklófeszítés
## Nyakkendő fogás
### Kokki - _2 db_
- Beszúrós karfeszítés
- Térdharapó Z-kulcs
### Donchigi - _1 db_
- Belső két mancs
## Fordított nyakkendő fogás
### Kokki - _3 db_
- Beszúrós karfeszítés
- Térdharapó Z-kulcs
- Sikoly
## Borda fogás
### Chigi - _1 db_
- Rákönyöklő
### Chagi - _1 db_
- Yop Chagi Hadan
### Kokki - _1 db_
- Bicepszledöntés
## Övfogás felülről
### Chigi - _1 db_
- Kipp-kopp
### Kokki - _2 db_
- Térdfelrántó Z-kulcs
- Karfeszítés oldalról
### Donchigi - _1 db_
- Belső két mancs
## Övfogás alulról
### Kokki - _3 db_
- Fordított csukló dobás
- Hüvelykujj letolás
- Átlós karfeszítés
### Donchigi
- Átlós karfeszítő dobás
