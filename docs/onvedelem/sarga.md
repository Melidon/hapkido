# 8. gup
## Azonos oldali csuklófogás
### Szabadulások - _5 db_
- Szabadulás lefelé
- Köríves szabadulás
- Szabadulás felfelé
- Átbújós szabadulás
- Gipszes kezű
### Chigi - _5 db_
- Könyök
- Nyak
- Halánték
- Másik borda
- Tengerész
### Kokki - _5 db_
- Tol-húz
- Ujjtörő karfeszítés
- Police
- Négyirányú dobás
- Térdharapó Z-kulcs
### Donchigi - _5+1 db_
- Gáncsledöntés
- Külső zsák
- Csípő dobás
- Váll dobás
- Kézcserés kereszcsípő dobás
- _Belső két mancs_
